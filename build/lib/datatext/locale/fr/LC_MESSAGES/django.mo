��    o      �  �         `	     a	     n	     u	  
   }	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	     �	     �	     
     	
     
  
   
     "
     4
     @
     I
     U
     ]
     d
  
   t
  	   
     �
     �
     �
  	   �
     �
  
   �
     �
     �
  5   �
          !     '     6     D     [     g     s          �     �  
   �  	   �  	   �     �  	   �     �     �     �       
              3  	   8  
   B  
   M  
   X  	   c  	   m     w     {     �  	   �     �     �     �     �     �     �     �     �     �     
          "     (     .     4     :     @     E     K     Q     W     ]     c     i     o     t     y     ~     �     �     �     �     �     �  
   �  
   �     �     �     �  j  �     F     \     c  
   k     v     �     �     �  	   �  
   �     �  
   �     �     �               ,     3     9     B     S     o     ~     �     �     �     �     �     �     �     �     �               *     7     ?  .   H     w     }     �     �     �     �     �     �     �                  	   .  
   8     C     I     f     r     ~     �     �  	   �     �     �     �     �       	        "     ;     ?     C     V     i     o     {     �     �     �     �     �     �     �     �               "     /     ;     H     J     L     O     S     X     \     _     c     f     j     m     o     r     v     {     ~     �     �     �     �     �     �                  E                 J   >           O   P      Y   M       =      a   '   :   U   "   !         h              j   L   S   2      Q   T               8           *       +   I      (   )                  \   B   4   %          _          b   A   c      d   $   e       f   m   F          -   /   5   V   i       Z   X      ]   ^      `   @   k   n   .   0      G          o          ;   K   ,           W   l   [   ?                  3       g       6   C                1   R       <   
       &   H       7   	                  N       D      #   9    Auteur_copie Author Authors Avancement BibRef BibRefs Bibliographies Bibliography Catalog Catalogs Categories Category Codex Codices Contenu_sec Contenu_traduit Copies Copy DaTaText Date_copie Date_copie_hegire Date_rajout Explicit First folio Incipit Italie Langue du texte Last folio Libraries Library Lien_web Modification des données Nb_folios Numincorpus References Section Sections Si le texte est référencé dans d'autres catalogues Text Texts Titre_sec_trad Titre_section abbreviation_catalogue annee_deces annee_naiss annee_texte ante auteur bibliotheque biographie catalogue categorie circa deces_hej desc_etape_avancement description_catalogue document_bibliog etape_avancement infor_supp intitule_catalogue lang lattitude lieu_deces lieu_naiss lieu_texte longitude naiss_hej nom nom_categorie nom_lieu_trans nom_trans pages_catalogue pays_actuel post precision_annee prenom prenom_trans pub_date ref_catalogue region_hist region_hist_trans s. -1 s. -2 s. -3 s. -4 s. -5 s. -6 s. 1 s. 10 s. 11 s. 12 s. 13 s. 14 s. 15 s. 16 s. 2 s. 3 s. 4 s. 5 s. 6 s. 7 s. 8 s. 9 siecle texte titre_orig titre_trad titre_trans ville ville_actuelle Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-06-22 12:09+0100
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Auteur selon la copie Auteur Auteurs Avancement Référence bibliographique Référence bibliographique Bibliographies Bibliographie Catalogue Catalogues Catégories Catégorie Codice Codices Contenu de la section Contenu traduit Copies Copie DaTaText Date de la copie Date de la copie en Hégire Date du rajout Explicit Premier folio Incipit Italie Langue du texte Dernier folio Bibliothèques Bibliothèque Lien web Modification des données Nombre de folios Numéro dans le corpus Références Section Sections Si le texte est référencé dans un catalogue Texte Textes intitulé traduit Intitulé de la section Abbréviation de l'intitulé Année du décès Année de la naissance Année ante Auteur Bibliothèque Éléments biographiques Catalogue Catégorie circa Année du décès en hégire Description Description Document bibliographique Intitulé de l'étape Information supplémentaire Intitulé Langue Latitude Lieu de décès Lieu de naissance Lieu de rédaction Longitude Année naiss. en hégire Nom Nom Nom translittéré Nom translittéré Pages Pays actuel post Précision de l'année Prénom Prénom translittéré Date de publication Référence Région historique Translittération I av. J.-C. II av. J.-C. III av. J.-C. IV av. J.-C. V av. J.-C. VI av. J.-C. I X XI XII XIII XIV XV XVI II III IV V VI VII VIII IX Siècle Texte Titre original Titre traduit Titre translittéré Ville historique Ville actuelle 