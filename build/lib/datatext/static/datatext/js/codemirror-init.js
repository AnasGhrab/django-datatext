(function(){
    var $ = django.jQuery;
    var tags = {
        "!top": ["p"],
        p: {
          children: ["persName", "placeName"]
        },
        persName: {
          attrs: {
            key: null,
            role: ["re","barone","conte"]
          },
          children: ["forename", "surname", "placeName"]
        },
        placeName: {
          children: ["settlement", "region", "country"]
        }
      };

    $(document).ready(function(){
        $('textarea.html-editor').each(function(idx, el){
            CodeMirror.fromTextArea(el, {
                lineNumbers: true,
                lineWrapping: true,
                //rtlMoveVisually: false,
                mode: 'htmlmixed',
		matchTags: {bothTags: true},
		autoCloseTags : true,
		foldGutter : true,
		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
		//viewportMargin: Infinity,
		extraKeys: {
			"Ctrl-J": "toMatchingTag",
 			"'<'": 'completeAfter',
			"'/'": 'completeIfAfterLt',
			"' '": 'completeIfInTag',
			"'='": 'completeIfInTag',
			"Ctrl-Space": "autocomplete",
			},
		hintOptions: {schemaInfo: tags},
            });
        });
    });
})();
