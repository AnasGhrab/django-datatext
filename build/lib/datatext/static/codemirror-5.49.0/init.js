(function(){
    var $ = django.jQuery;
    $(document).ready(function(){
        $('textarea.html-editor').each(function(idx, el){
            CodeMirror.fromTextArea(el, {
                lineNumbers: true,
                mode: 'htmlmixed',
		matchTags: {bothTags: true},
		extraKeys: {"Ctrl-J": "toMatchingTag"},
		autoCloseTags : true,
		foldGutter : true,
		gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
            });
        });
    });
})();
