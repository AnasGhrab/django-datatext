#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.apps import AppConfig

class DataTextConfig(AppConfig):
    name = 'datatext'
    verbose_name = _('DaTaText')
