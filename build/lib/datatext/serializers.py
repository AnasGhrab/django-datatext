from rest_framework import serializers
import datatext.models.models as M
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
    auteurs = serializers.PrimaryKeyRelatedField(many=True, queryset=M.Auteur.objects.all())
    
    class Meta:
        model = User
        fields = ['id', 'username', 'auteurs']

class AuteurSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = M.Auteur
        fields = '__all__'
        #fields = ['nom', 'prenom', 'lieu', 'owner']

class TexteSerializer(serializers.ModelSerializer):
    owner = serializers.ReadOnlyField(source='owner.username')

    class Meta:
        model = M.Texte
        fields = '__all__'
 
