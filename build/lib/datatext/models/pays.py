#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _
from django.db import models

class Pays(models.Model):
    nom = models.CharField(_('nom_pays'),max_length=40,null=True,blank=True,help_text=_('Nom pays'))
    nom_fr = models.CharField(_('nom_pays_fr'),max_length=40,null=True,blank=True,help_text=_('Nom pays fr'))
    nom_en = models.CharField(_('nom_pays_en'),max_length=40,null=True,blank=True,help_text=_('Nom pays en'))
    code = models.CharField(_('nom_pays_en'),max_length=3,null=True,blank=True,help_text=_('Nom pays en'))
    determination = models.BooleanField(default=False)

    class Meta:
        managed = True
        db_table = 'sara_pays'
        verbose_name = _('Pays')
        verbose_name_plural = _('Pays')
        ordering = ['nom']

    def __str__(self):              # __unicode__ on Python 2
        return self.nom

class Ville(models.Model):
    nom = models.CharField(_('nom_ville'),max_length=20,null=True,blank=True,help_text=_('Nom ancien de la ville'))
    nom_actuel = models.CharField(_('nom_ville'),max_length=20,null=True,blank=True,help_text=_('Nom actuel de la ville'))
    nom_fr = models.CharField(_('nom_ville_fr'),max_length=20,null=True,blank=True,help_text=_('Nom ancien de la ville - fr'))
    nom_fr_actuel = models.CharField(_('nom_ville_fr'),max_length=20,null=True,blank=True,help_text=_('Nom actuel de la ville - fr'))
    nom_en = models.CharField(_('nom_ville_en'),max_length=20,null=True,blank=True,help_text=_('Nom ancien de la ville - en'))
    nom_en_actuel = models.CharField(_('nom_ville_en'),max_length=20,null=True,blank=True,help_text=_('Nom actuel de la ville - en'))
    pays = models.ForeignKey(Pays, null=True,blank=True,help_text=_('Pays actuel'), on_delete=models.SET_NULL)
    lat = models.FloatField(_('lattitude'),null=True,blank=True)
    longi = models.FloatField(_('longitude'),null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'sara_ville'
        verbose_name = _('Ville')
        verbose_name_plural = _('Villes')
        ordering = ['nom']

    def __str__(self):              # __unicode__ on Python 2
        return '%s (%s)' % (self.nom, self.pays)
