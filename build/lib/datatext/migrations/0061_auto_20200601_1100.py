# Generated by Django 3.0.6 on 2020-06-01 10:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0060_texte_lieu2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ville',
            name='lat',
            field=models.FloatField(blank=True, null=True, verbose_name='latitude'),
        ),
    ]
