# Generated by Django 2.2.7 on 2019-12-14 17:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0006_auto_20191128_2015'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pays',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nom', models.CharField(blank=True, help_text='Nom pays', max_length=100, null=True, verbose_name='nom_pays')),
                ('nom_fr', models.CharField(blank=True, help_text='Nom pays', max_length=100, null=True, verbose_name='nom_pays')),
                ('nom_en', models.CharField(blank=True, help_text='Nom pays', max_length=100, null=True, verbose_name='nom_pays')),
            ],
            options={
                'managed': True,
                'db_table': 'sara_pays',
                'verbose_name_plural': 'Pays',
                'verbose_name': 'Pays',
            },
        ),
        migrations.AddField(
            model_name='bibliotheque',
            name='pays2',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datatext.Pays'),
        ),
    ]
