# Generated by Django 2.1.2 on 2018-11-02 17:59

from django.db import migrations
import taggit.managers


class Migration(migrations.Migration):

   dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('datatext', '0045_auto_20181101_1825'),
    ]

   operations = [
       migrations.AddField(
            model_name='section',
            name='tags',
            field=taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
       migrations.AddField(
            model_name='texte',
            name='tags',
            field=taggit.managers.TaggableManager(blank=True, help_text='A comma-separated list of tags.', through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
   ]
