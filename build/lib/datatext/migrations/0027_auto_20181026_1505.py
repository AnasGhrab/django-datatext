# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2018-10-26 15:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0026_auto_20181026_1503'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='texte',
            options={'managed': True, 'ordering': ['titre_orig'], 'verbose_name': 'Texte', 'verbose_name_plural': 'Textes'},
        ),
    ]
