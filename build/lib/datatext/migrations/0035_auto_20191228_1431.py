# Generated by Django 2.2.7 on 2019-12-28 13:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0034_auto_20191228_1425'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ville',
            options={'managed': True, 'ordering': ['nom'], 'verbose_name': 'Ville', 'verbose_name_plural': 'Villes'},
        ),
    ]
