# Generated by Django 2.2.7 on 2020-01-11 20:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0042_auto_20200111_2136'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bibliotheque',
            name='ville',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='datatext.Ville'),
        ),
    ]
