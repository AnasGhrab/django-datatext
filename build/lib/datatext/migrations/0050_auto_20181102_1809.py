# Generated by Django 2.1.2 on 2018-11-02 18:09

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0049_auto_20181102_1804'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='auteur',
            options={'managed': True, 'ordering': ('nom',), 'verbose_name': 'مؤلّف', 'verbose_name_plural': 'مؤلّفون'},
        ),
        migrations.AlterModelOptions(
            name='avancement',
            options={'managed': True, 'verbose_name': 'مستوى النّشر', 'verbose_name_plural': 'مستوى النّشر'},
        ),
        migrations.AlterModelOptions(
            name='bibliographie',
            options={'managed': True, 'verbose_name': 'مرجع', 'verbose_name_plural': 'المراجع البيبلوغرافيّة'},
        ),
        migrations.AlterModelOptions(
            name='bibliotheque',
            options={'managed': True, 'verbose_name': 'مكتبة', 'verbose_name_plural': 'المكتبات'},
        ),
        migrations.AlterModelOptions(
            name='catalogue',
            options={'managed': True, 'verbose_name': 'فهرس', 'verbose_name_plural': 'فهارس'},
        ),
        migrations.AlterModelOptions(
            name='categorie',
            options={'managed': True, 'verbose_name': 'مجال', 'verbose_name_plural': 'المجالات'},
        ),
        migrations.AlterModelOptions(
            name='copie',
            options={'managed': True, 'verbose_name': 'نسخة', 'verbose_name_plural': 'النّسخ'},
        ),
        migrations.AlterModelOptions(
            name='corpus',
            options={'managed': True, 'verbose_name': 'مجموعة', 'verbose_name_plural': 'المجموعات'},
        ),
        migrations.AlterModelOptions(
            name='lieu',
            options={'managed': True, 'verbose_name': 'موقع', 'verbose_name_plural': 'المواقع التاريخيّة'},
        ),
        migrations.AlterModelOptions(
            name='refbib',
            options={'managed': True, 'verbose_name': 'الإشارة إلى المرجع', 'verbose_name_plural': 'الإشارات البيبلوغرافيّة'},
        ),
        migrations.AlterModelOptions(
            name='reference',
            options={'managed': True, 'verbose_name': 'الإشارات', 'verbose_name_plural': 'الإشارات'},
        ),
        migrations.AlterModelOptions(
            name='section',
            options={'managed': True, 'verbose_name': 'جزء', 'verbose_name_plural': 'الأجزاء'},
        ),
        migrations.AlterModelOptions(
            name='texte',
            options={'managed': True, 'ordering': ['titre_orig'], 'verbose_name': 'نصّ', 'verbose_name_plural': 'نصوص'},
        ),
        migrations.AlterField(
            model_name='auteur',
            name='biographie',
            field=models.TextField(blank=True, null=True, verbose_name='معطيات بيوغرافيّة'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='deces_date',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='سنة الوفاة'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='deces_hej',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='سنة الوفاة (هـ.)'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='deces_lieu',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='مكان الوفاة'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='naiss_date',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='سنة الولادة'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='naiss_hej',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='سنة الولادة (هـ.)'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='naiss_lieu',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='مكان الولادة'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='nom',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='اللّقب'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='nom_trans',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='اللّقب بالأحرف اللّاتينيّة'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='prenom',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='الإسم'),
        ),
        migrations.AlterField(
            model_name='auteur',
            name='prenom_trans',
            field=models.CharField(blank=True, max_length=80, null=True, verbose_name='الإسم بالأحرف اللّاتينيّة'),
        ),
        migrations.AlterField(
            model_name='avancement',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='وصف مستوى التّقدّم'),
        ),
        migrations.AlterField(
            model_name='avancement',
            name='titre',
            field=models.CharField(max_length=100, verbose_name='التّقدّم في النّشر'),
        ),
        migrations.AlterField(
            model_name='catalogue',
            name='abbrev',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='abbreviation_catalogue'),
        ),
        migrations.AlterField(
            model_name='catalogue',
            name='description',
            field=models.TextField(blank=True, null=True, verbose_name='وصف الفهرس'),
        ),
        migrations.AlterField(
            model_name='catalogue',
            name='intitule',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='اسم الفهرس'),
        ),
        migrations.AlterField(
            model_name='categorie',
            name='intitule',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='اسم المجال'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='auteur',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='المؤلّف حسب النّسخة'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='commentaires',
            field=models.TextField(blank=True, null=True, verbose_name='معطيات إضافيّة'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='copiste',
            field=models.CharField(blank=True, max_length=60, null=True, verbose_name='النّاسخ'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='date',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='تاريخ النّسخ'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='date_hej',
            field=models.CharField(blank=True, max_length=40, null=True, verbose_name='تاريخ النّسخ (هـ.)'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='explicit',
            field=models.CharField(blank=True, max_length=400, null=True, verbose_name='النّهاية'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='folio_begin',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='الورقة الأولى'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='folio_end',
            field=models.CharField(blank=True, max_length=10, null=True, verbose_name='الورقة الأخيرة'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='folios',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='عدد الورقات'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='id_texte',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datatext.Texte', verbose_name='النّصّ'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='incipit',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='البداية'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='lieu',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='مكان النّسخ'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='numincorpus',
            field=models.IntegerField(blank=True, null=True, verbose_name='الرّقم بالمجموعة'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='pub_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='تاريخ الإضافة'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='titre',
            field=models.CharField(blank=True, max_length=70, null=True, verbose_name='العنوان حسب النّسخة'),
        ),
        migrations.AlterField(
            model_name='copie',
            name='url',
            field=models.URLField(blank=True, null=True, verbose_name='رابط على الواب'),
        ),
        migrations.AlterField(
            model_name='corpus',
            name='id_biblio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datatext.Bibliotheque', verbose_name='bibliotheque'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='lat',
            field=models.FloatField(blank=True, null=True, verbose_name='العرض الجغرافي'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='longi',
            field=models.FloatField(blank=True, null=True, verbose_name='لطّول الجغرافي'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='nom_trans',
            field=models.CharField(blank=True, help_text='اسم المكان بالأحرف اللّاتينيّة', max_length=100, null=True, verbose_name='اسم المكان بالأحرف اللّاتينيّة'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='pays',
            field=models.CharField(blank=True, choices=[('DZ', 'الجزائر'), ('DE', 'ألمانيا'), ('EG', 'مصر'), ('ES', 'اسبانيا'), ('FR', 'فرنسا'), ('GR', 'اليونان'), ('IT', 'إيطاليا'), ('IQ', 'العراق'), ('LB', 'لبنان'), ('LY', 'ليبيا'), ('MK', 'مقدونيا'), ('TR', 'تركيا'), ('TN', 'تونس'), ('SY', 'سوريا')], help_text='البلد الحالي', max_length=5, null=True, verbose_name='البلد'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='region',
            field=models.CharField(blank=True, help_text='اسم المنطقة التّاريخيّة بالأحرف العربيّة', max_length=100, null=True, verbose_name='المنطقة التّاريخيّة'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='region_trans',
            field=models.CharField(blank=True, help_text='الاسم التاريخي بالأحرف اللّاتينيّة', max_length=100, null=True, verbose_name='بالأحرف اللّاتينيّة'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='ville',
            field=models.CharField(blank=True, help_text='إن كانت مغايرة للاسم التّاريخي', max_length=100, null=True, verbose_name='الاسم الحالي للبلدة'),
        ),
        migrations.AlterField(
            model_name='lieu',
            name='ville_ancienne',
            field=models.CharField(max_length=100, null=True, verbose_name='البلدة'),
        ),
        migrations.AlterField(
            model_name='refbib',
            name='texte',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datatext.Texte', verbose_name='النّصّ'),
        ),
        migrations.AlterField(
            model_name='refbib',
            name='zotero',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='datatext.Bibliographie', verbose_name='document_bibliog'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='catalogue',
            field=models.ForeignKey(blank=True, help_text='إن كان النّص قد ذكر بفهارس أخرى', null=True, on_delete=django.db.models.deletion.CASCADE, to='datatext.Catalogue', verbose_name='catalogue'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='numero',
            field=models.IntegerField(blank=True, help_text='Référence dans le catalogue (si existe)', null=True, verbose_name='رقم'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='pages',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='pages_catalogue'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='texte',
            field=models.ForeignKey(blank=True, help_text='إن كان النّص قد ذكر بفهارس أخرى', null=True, on_delete=django.db.models.deletion.CASCADE, to='datatext.Texte', verbose_name='النّصّ'),
        ),
        migrations.AlterField(
            model_name='responsable',
            name='texte',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datatext.Texte', verbose_name='النّصّ'),
        ),
        migrations.AlterField(
            model_name='section',
            name='contenu',
            field=models.TextField(blank=True, help_text='Contenu avec des balises <a href="http://www.tei-c.org/Guidelines/P5/index.xml">TEI</a>. Ne doit contenir que les balises du niveau paragraphe, ou un niveau similaire, à l\'intérieur d\'une balise <a href ="http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-div.html"><b>div</b></a>', null=True, verbose_name='محتوى الجزء'),
        ),
        migrations.AlterField(
            model_name='section',
            name='pub_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='تاريخ الإضافة'),
        ),
        migrations.AlterField(
            model_name='section',
            name='texte',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datatext.Texte', verbose_name='النّصّ'),
        ),
        migrations.AlterField(
            model_name='section',
            name='titre',
            field=models.CharField(max_length=200, null=True, verbose_name='اسم الجزء'),
        ),
        migrations.AlterField(
            model_name='section',
            name='titre_trad',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='ترجمة لاسم الجزء'),
        ),
        migrations.AlterField(
            model_name='section',
            name='traduction',
            field=models.TextField(blank=True, help_text='يتبع المحتوى تشفيرات TEI', null=True, verbose_name='ترجمة المحتوى'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='annee',
            field=models.IntegerField(blank=True, help_text='Si connue', null=True, verbose_name='السّنة'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='auteur',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='datatext.Auteur', verbose_name='المؤلّف'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='avancement',
            field=models.ManyToManyField(blank=True, help_text='مستوى التّقدّم في المعطيات الرّقميّة', to='datatext.Avancement', verbose_name='التّقدّم'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='categorie',
            field=models.ManyToManyField(blank=True, help_text='حقل غير مفيد ؟ يختلف عن كلمات الفهرسة...', to='datatext.Categorie', verbose_name='مجال'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='commentaires',
            field=models.TextField(blank=True, help_text='حقل غير مفيد ؟', null=True, verbose_name='معطيات إضافيّة'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='lang',
            field=models.CharField(choices=[('ar', 'Arabe')], default='ar', help_text='لغة النّص', max_length=3, verbose_name='اللّغة'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='lieu',
            field=models.ForeignKey(blank=True, help_text='البلدة الّتي يرجّح أن يكون قمّ تمّ بها تأليف النّصّ', null=True, on_delete=django.db.models.deletion.CASCADE, to='datatext.Lieu', verbose_name='مكان التأليف'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='precision',
            field=models.CharField(blank=True, choices=[('c', 'تقريبا'), ('a', 'قبل'), ('p', 'بعد')], max_length=1, null=True, verbose_name='دقّة التّاريخ'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='pub_date',
            field=models.DateTimeField(default=django.utils.timezone.now, verbose_name='تاريخ الإضافة'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='siecle',
            field=models.IntegerField(choices=[(-3000, 'troisi. mill. av. J.-C.'), (-2000, 'deuxi. mill. av. J.-C.'), (-1000, 'premi. mill. av. J.-C.'), (-6, 'القرن 6 ق. م.'), (-5, 'القرن 5 ق. م.'), (-4, 'القرن 4 ق. م.'), (-3, 'القرن 3 ق. م.'), (-2, 'القرن 2 ق. م.'), (-1, 'القرن 1 ق. م.'), (1, 'القرن الأوّل ميلادي'), (2, 'القرن الثّاني م.'), (3, 'القرن الثّالث م.'), (4, 'القرن الرّابع م.'), (5, 'القرن الخامس م.'), (6, 'القرن السّادس م.'), (7, 'القرن السّابع م.'), (8, 'القرن الثّامن م.'), (9, 'القرن التّاسع م.'), (10, 'القرن العاشر م.'), (11, 'القرن الحادي عشر م.'), (12, 'القرن الثّاني عشر م.'), (13, 'القرن الثّالث عشر م.'), (14, 'القرن الرّابع عشر م.'), (15, 'القرن الخامس عشر م.'), (16, 'القرن السّادس عشر م.')], null=True, verbose_name='القرن'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='titre_orig',
            field=models.CharField(help_text='باستعمال الأحرف العربيّة', max_length=150, verbose_name='العنوان باللّغة الأصليّة'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='titre_trad',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='العنوان مترجم'),
        ),
        migrations.AlterField(
            model_name='texte',
            name='titre_trans',
            field=models.CharField(blank=True, help_text='Arabe : ISO 233-2:1993; Grec : ISO 843:1997; Hébreu : ISO 259-2:1994', max_length=100, null=True, verbose_name='العنوان بالأحرف اللّاتينيّة'),
        ),
    ]
