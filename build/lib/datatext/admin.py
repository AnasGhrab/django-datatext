#!/usr/local/bin/python
# -*- coding: utf-8 -*-

#from __future__ import unicode_literals
from django.contrib import admin
from datatext.models.models import *
from .models.dateslieux import *
from .models.pays import *

from django.utils.translation import ugettext as _


from datatext.widgets import *
from django.forms import TextInput, Textarea
from django import forms

admin.site.site_header = _('Modification des données')

class BasiclogAdmin(admin.ModelAdmin):
	exclude = ['created_by', 'modified_by']
	def save_model(self, request, obj, form, change):
	   if not obj.pk:
		   obj.created_by = request.user
	   obj.modified_by = request.user
	   super().save_model(request, obj, form, change)


class SectionInline(admin.StackedInline):
	model = Section

class RefInline(admin.StackedInline):
	model = Reference
	extra = 1

# Ancienne gestion pour Zotero
class BibInline(admin.StackedInline):
	model = RefBib

class BibliographicInline(admin.StackedInline):
	model = Bibliographic
	extra = 1
	exclude = ['created_by', 'modified_by']

class ResponsableInline(admin.StackedInline):
	model = Responsable
	extra = 1

class TextAdmin(BasiclogAdmin):
	list_display = ['id', 'titre_orig','titre_trad', 'auteur','siecle','annee','precision','lieu', 'highlight', 'created_by', 'created_on', 'modified_by', 'modified_on']
	ordering = ['-id', 'titre_orig','siecle','auteur__nom']
	list_filter = ('categorie','siecle','lieu','highlight')
	search_fields = ['titre_orig','auteur__prenom', 'auteur__nom']
	inlines = [ ResponsableInline, RefInline, BibliographicInline]

class SectionAdminForm(forms.ModelForm):
    model = Section
    class Meta:
        fields = '__all__'
        widgets = {
                'titre': TextInputAdmin(attrs={'style': 'height: auto;',}),
                'titre_trad': InputLTRAdmin(attrs={'style': 'width: 100%; height: 2%;'}),
                'contenu': HtmlEditor(attrs={'lineWrapping':'true'}),
                'traduction': LTRTextAdmin(),
                }

class SectionAdmin(BasiclogAdmin):
	def texte__siecle(self, obj):
		return obj.texte.siecle

	list_display = ['titre', 'titre_trad', 'texte', 'texte__siecle', 'created_by', 'created_on', 'modified_by', 'modified_on']
	search_fields = ['texte__titre_orig','texte__auteur__nom','texte__auteur__prenom', 'titre', 'titre_trad', 'contenu', 'traduction']
	autocomplete_fields = ['texte', 'parent']
	list_filter = ['texte__siecle']
	ordering = ['texte__titre_orig']
	form = SectionAdminForm

class AuteurAdmin(BasiclogAdmin):
	search_fields = ['nom','prenom']
	list_display = ['prenom','nom', 'lieu', 'naiss_date','deces_date','naiss_lieu2', 'deces_lieu2', 'naiss_lieu','deces_lieu', 'modified_by', 'modified_on']
	ordering = ['prenom',]

class ResponsableAdmin(admin.ModelAdmin):
	def get_model_perms(self, request):
		"""
		Return empty perms dict thus hiding the model from admin index.
		"""
		return {}

class FonctionAdmin(admin.ModelAdmin):
	def get_model_perms(self, request):
		"""
		Return empty perms dict thus hiding the model from admin index.
		"""
		return {}

class CatalogueAdmin(admin.ModelAdmin):
	list_display = ['intitule','abbrev']

class BibliographicAdmin(BasiclogAdmin):
    list_display = ['texte','notice', 'created_by', 'created_on', 'modified_by', 'modified_on']
    autocomplete_fields = ['notice']

class CopieAdmin(BasiclogAdmin):
        search_fields = ['id_corpus__prefix', 'id_corpus__cote', 'id_texte__titre_orig', 'id_texte__auteur__prenom', 'id_texte__auteur__nom']
        list_display = ['id_texte', 'id_corpus', 'created_by', 'created_on', 'modified_by', 'modified_on']
        list_filter = ['id_corpus__id_biblio__ville']
        autocomplete_fields = ['id_texte', 'id_corpus']

class CorpusAdmin(BasiclogAdmin):
        search_fields = ['id_biblio__nom', 'cote', 'id_biblio__ville__nom', 'id_biblio__ville__pays__nom']
        list_display = ['prefix', 'cote', 'id_biblio', 'created_by', 'created_on', 'modified_by', 'modified_on']
        list_filter=['id_biblio__ville__pays__nom', 'id_biblio__ville', 'id_biblio__nom']
        autocomplete_fields = ['id_biblio']

class BibliothequeAdmin(BasiclogAdmin):
        search_fields = ['nom', 'ville__nom', 'ville__pays__nom']
        list_display = ['nom','ville', 'longitude', 'latitude', 'url']
        list_filter = ['pays']
        autocomplete_fields = ['ville']

class PaysAdmin(admin.ModelAdmin):
	list_display = ['nom','code', 'nom_fr', 'nom_en']

class VilleAdmin(admin.ModelAdmin):
	list_display = ['nom','pays', 'lat', 'longi']
	list_filter = ['pays']
	search_fields = ['pays', 'nom']

class ReferenceAdmin(admin.ModelAdmin):
	list_display = ['texte','numero', 'pages']
	list_filter = ['catalogue',]

class IconotagsAdmin(admin.ModelAdmin):
        list_display = ['tag']
        def has_module_permission(self, request):
            return False

class IconoAdmin(BasiclogAdmin):
        list_display = ['intitule','copie', 'folio', 'x', 'y', 'x_pixels', 'y_pixels']
        #list_filter = ['copie']
        autocomplete_fields = ['copie']

admin.site.register(Auteur,AuteurAdmin)
admin.site.register(Responsable,ResponsableAdmin)
admin.site.register(Fonction,FonctionAdmin)
#admin.site.register(Lieu)
admin.site.register(Texte,TextAdmin)
admin.site.register(Catalogue,CatalogueAdmin)
admin.site.register(Reference,ReferenceAdmin)
admin.site.register(Copie, CopieAdmin)
admin.site.register(Section,SectionAdmin)
admin.site.register(Categorie)
admin.site.register(Corpus, CorpusAdmin)
admin.site.register(Bibliotheque, BibliothequeAdmin)
admin.site.register(Bibliographic, BibliographicAdmin)
#admin.site.register(Bibliographie)
admin.site.register(Avancement)
admin.site.register(Pays, PaysAdmin)
admin.site.register(Ville, VilleAdmin)
admin.site.register(Iconotags, IconotagsAdmin)
admin.site.register(Icono, IconoAdmin)
