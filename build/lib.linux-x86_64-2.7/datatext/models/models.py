#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models
from datetime import datetime
from django.conf import settings
from dateslieux import Lieu
import dateslieux as s
from taggit.managers import TaggableManager
from django.contrib.auth.models import User


class Auteur(models.Model):
    nom = models.CharField(_('nom'),max_length=50,null=True,blank=True)
    prenom = models.CharField(_('prenom'),max_length=100,null=True,blank=True)
    nom_trans = models.CharField(_('nom_trans'),max_length=40,null=True,blank=True)
    prenom_trans = models.CharField(_('prenom_trans'),max_length=80,null=True,blank=True)
    naiss_date = models.CharField(_('annee_naiss'),max_length=10,null=True,blank=True)
    deces_date = models.CharField(_('annee_deces'),max_length=10,null=True,blank=True)
    naiss_hej = models.CharField(_('naiss_hej'),max_length=10,null=True,blank=True)
    deces_hej = models.CharField(_('deces_hej'),max_length=10,null=True,blank=True)
    naiss_lieu = models.CharField(_('lieu_naiss'),max_length=20,null=True,blank=True)
    deces_lieu = models.CharField(_('lieu_deces'),max_length=20,null=True,blank=True)
    biographie = models.TextField(_('biographie'),null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_auteurs'
        verbose_name = _('Author')
        verbose_name_plural = _('Authors')

    def __unicode__(self):              # __unicode__ on Python 2
        #return '%s %s' % (self.prenom_ar, self.nom_ar)
        return '%s %s' % (self.prenom, self.nom)

class Categorie(models.Model):
    intitule = models.CharField(_('nom_categorie'),max_length=100,null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_textes_categories'
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __unicode__(self):              # __unicode__ on Python 2
        return self.intitule

class Avancement(models.Model):
    titre = models.CharField(_('etape_avancement'),max_length=100)
    description = models.TextField(_('desc_etape_avancement'),null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'sara_avancement'
        verbose_name = _('Avancement')
        verbose_name_plural = _('Avancement')

    def __unicode__(self):              # __unicode__ on Python 2
        return self.titre

class Texte(models.Model):
    LANGUES = (
	    (settings.LANG_TXT_DEFAULT),
#            ('ar', _('arabe')),
#            ('fr', _('français')),
#	    ('gr', _('grec')),
#	    ('heb', _('hebreu')),
#	    ('it', _('italien')),
#	    ('lat', _('latin')),
#	    ('per', _('persan')),
        )
    PRECISION = (
	    ('c', _('circa')),
	    ('a', _('ante')),
	    ('p', _('post')),
	)
    lang = models.CharField(_('lang'),max_length=3,choices=LANGUES,default=settings.LANG_TXT_DEFAULT[0],help_text=_('Langue du texte'))
    titre_orig = models.CharField(_('titre_orig'),max_length=150,help_text=_('En utilisant l\'alphabet de la langue'))
    titre_trans = models.CharField(_('titre_trans'),max_length=100,null=True,blank=True,help_text=_('Arabe : ISO 233-2:1993; Grec : ISO 843:1997; Hébreu : ISO 259-2:1994'))
    titre_trad = models.CharField(_('titre_trad'),max_length=100,null=True,blank=True)
    if (settings.NB_TRAD > 1):
	titre_trad2 = models.CharField(_('titre_trad2'),help_text=_('Traduction du titre dans une deuxième langue'),max_length=100,null=True,blank=True)
    siecle = models.IntegerField(_('siecle'),null=True, choices=s.SIECLES)
    annee = models.IntegerField(_('annee_texte'),null=True, blank=True,help_text=_('Si connue'))
    precision = models.CharField(_('precision_annee'),max_length=1,null=True,blank=True,choices=PRECISION)
    auteur = models.ForeignKey(Auteur,verbose_name=_('auteur'))
    lieu = models.ForeignKey(Lieu,verbose_name=_('lieu_texte'),null=True, blank=True,help_text=_('Lieu possible/probable de la rédaction du texte'))
    commentaires = models.TextField(_('infor_supp'),null=True,blank=True,help_text=_('Champ inutile ?'))
    categorie = models.ManyToManyField(Categorie,verbose_name=_('categorie'),blank=True,help_text=_('Champ nécessaire ? Différent des mots-clés ?'))
    tags = TaggableManager(blank=True)
    avancement = models.ManyToManyField(Avancement,verbose_name=_('avancement'),blank=True,help_text=_('État de l\'avancement dans la publication numérique'))
    pub_date = models.DateTimeField(_('pub_date'),default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_textes'
        verbose_name = _('Text')
        verbose_name_plural = _('Texts')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s (%s)' % (self.titre_orig, self.auteur)

class Catalogue(models.Model):
    intitule = models.CharField(_('intitule_catalogue'),max_length=100,null=True,blank=True)
    abbrev = models.CharField(_('abbreviation_catalogue'),max_length=10,null=True,blank=True)
    description = models.TextField(_('description_catalogue'),null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'sara_textes_catalogues'
        verbose_name = _('Catalog')
        verbose_name_plural = _('Catalogs')

    def __unicode__(self):              # __unicode__ on Python 2
        return ('%s (%s)') % (self.intitule,self.abbrev)


class Reference(models.Model):
    texte = models.ForeignKey(Texte,verbose_name=_('texte'), null=True,blank=True,help_text=_('Si le texte est référencé dans d\'autres catalogues'))
    catalogue = models.ForeignKey(Catalogue,verbose_name=_('catalogue'),null=True,blank=True,help_text=_('Si le texte est référencé dans d\'autres catalogues'))
    numero = models.IntegerField(_('ref_catalogue'),null=True,blank=True, help_text=_('Référence dans le catalogue (si existe)'))
    pages = models.CharField(_('pages_catalogue'),max_length=100,null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'sara_catalogues_references'
        verbose_name = _('References')
        verbose_name_plural = _('References')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s | %s | %s' % (self.texte, self.catalogue, self.numero)

class Bibliographie(models.Model):
    zotero = models.CharField(max_length=50,null=True,blank=True)
    lang = models.CharField(max_length=10,null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_bibliographie'
        verbose_name = _('Bibliography')
        verbose_name_plural = _('Bibliographies')

    def __unicode__(self):              # __unicode__ on Python 2
        return self.zotero

class RefBib(models.Model):
    texte = models.ForeignKey(Texte,verbose_name=_('texte'))
    zotero = models.ForeignKey(Bibliographie,verbose_name=_('document_bibliog'),null=True,blank=True)
    page = models.CharField(max_length=25,null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now,null=True,blank=True)

    class Meta:
        managed = True
        db_table = 'sara_bibref'
        verbose_name = _('BibRef')
        verbose_name_plural = _('BibRefs')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s, %s : %s' % (self.texte, self.zotero, self.page)

class Bibliotheque(models.Model):
    nom = models.CharField(max_length=100)
    nom_origin = models.CharField(max_length=100,blank=True)
    nom_abbrev = models.CharField(max_length=10)
    pays = models.CharField(max_length=40)
    ville = models.CharField(max_length=40)
    url = models.URLField(null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_bibliotheques'
        verbose_name = _('Library')
        verbose_name_plural = _('Libraries')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s : %s-%s' % (self.nom, self.pays, self.ville)

class Corpus(models.Model):
    id_biblio = models.ForeignKey(Bibliotheque,verbose_name=_('bibliotheque'))
    prefix = models.CharField(max_length=25,null=True,blank=True)
    cote = models.IntegerField()
    ancienne_cote = models.CharField(max_length=30,null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_corpus'
        verbose_name = _('Codex')
        verbose_name_plural = _('Codices')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s : %s%s' % (self.id_biblio, self.prefix, self.cote)

class Copie(models.Model):
    id_corpus = models.ForeignKey(Corpus,verbose_name=_('corpus'))
    # Translators: Copie manuscrit
    numincorpus = models.IntegerField(_('Numincorpus'),null=True,blank=True)
    id_texte = models.ForeignKey(Texte,verbose_name=_('texte'))
    folios = models.CharField(_('Nb_folios'),max_length=20,null=True,blank=True)
    folio_begin = models.CharField(_('First folio'),max_length=10,null=True,blank=True)
    folio_end = models.CharField(_('Last folio'),max_length=10,null=True,blank=True)
    auteur = models.CharField(_('Auteur_copie'),max_length=30,null=True,blank=True)
    titre = models.CharField(_('Titre_copie'),max_length=70,null=True,blank=True)
    copiste = models.CharField(_('Copiste'),max_length=60,null=True,blank=True)
    date_hej = models.CharField(_('Date_copie_hegire'),max_length=40,null=True,blank=True)
    date = models.CharField(_('Date_copie'),max_length=40,null=True,blank=True)
    lieu = models.CharField(_('Lieu_copie'),max_length=30,null=True,blank=True)
    commentaires = models.TextField(_('Info_supp'),null=True,blank=True)
    incipit = models.CharField(_('Incipit'),max_length=200,null=True,blank=True)
    explicit = models.CharField(_('Explicit'),max_length=400,null=True,blank=True)
    url = models.URLField(_('Lien_web'),null=True,blank=True)
    pub_date = models.DateTimeField(_('Date_rajout'),default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_copies_textes'
        verbose_name = _('Copy')
        verbose_name_plural = _('Copies')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s : %s' % (self.id_texte, self.id_corpus)

class Section(models.Model):
    texte = models.ForeignKey(Texte,verbose_name=_('texte'))
    # Translators: Les sections du textes
    titre = models.CharField(_('Titre_section'),max_length=200,null=True,blank=False)
    titre_trad = models.CharField(_('Titre_sec_trad'),max_length=200,null=True,blank=True)
    parent = models.ForeignKey("self",null=True,blank=True,help_text=_('Section parente, si elle existe'))
    contenu = models.TextField(_('Contenu_sec'),null=True,blank=True,help_text=_('Contenu avec des balises <a href="http://www.tei-c.org/Guidelines/P5/index.xml">TEI</a>. Ne doit contenir que les balises du niveau paragraphe, ou un niveau similaire, à l\'intérieur d\'une balise <a href ="http://www.tei-c.org/release/doc/tei-p5-doc/en/html/ref-div.html"><b>div</b></a>'))
    traduction = models.TextField(_('Contenu_traduit'),null=True,blank=True,help_text=_('Contenu avec des balises <a href="http://www.tei-c.org/Guidelines/P5/index.xml">TEI</a>'))
    if (settings.NB_TRAD > 1):
        titre_trad2 = models.CharField(_('Titre_sec_trad2'),max_length=200,null=True,blank=True,help_text=_('Traduction de l\'intitulé de la section dans une deuxième langue'))
	traduction2 = models.TextField(_('Contenu_traduit2'),null=True,blank=True,help_text=_('Traduction de la section dans une deuxième langue'))
    tags = TaggableManager(blank=True)
    pub_date = models.DateTimeField(_('Date_rajout'),default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_sections'
        verbose_name = _('Section')
        verbose_name_plural = _('Sections')

    def __unicode__(self):              # __unicode__ on Python 2
        return '%s : %s' % (self.texte, self.titre)

class Fonction(models.Model):
        intitule = models.CharField(_('fonction'),max_length=50,null=True,blank=True)

    	class Meta:
        	managed = True
        	db_table = 'sara_fonctions'
        	verbose_name = _('Fonction')
        	verbose_name_plural = _('Fonctions')

    	def __unicode__(self):              # __unicode__ on Python 2
        	return '%s' % (self.intitule)

class Responsable(models.Model):
        texte = models.ForeignKey(Texte,verbose_name=_('texte'))
        responsable = models.ForeignKey(User,verbose_name=_('responsable'))
        fonction = models.ForeignKey(Fonction,verbose_name=_('fonction'))

        class Meta:
                managed = True
                db_table = 'sara_responsables'
                verbose_name = _('Mention de responsabilité')
                verbose_name_plural = _('Mentions de responsabilité')

        def __unicode__(self):              # __unicode__ on Python 2
                return '%s (%s) : %s' % (self.responsable, self.fonction, self.texte)
