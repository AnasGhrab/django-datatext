#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models
from datetime import datetime

SIECLES = (
        (-3000, _('troisi. mill. av. J.-C.')),
        (-2000, _('deuxi. mill. av. J.-C.')),
        (-1000, _('premi. mill. av. J.-C.')),
	(-6, _('s. -6')),
	(-5, _('s. -5')),
	(-4, _('s. -4')),
	(-3, _('s. -3')),
	(-2, _('s. -2')),
	(-1, _('s. -1')),
	(1, _('s. 1')),
	(2, _('s. 2')),
	(3, _('s. 3')),
	(4, _('s. 4')),
	(5, _('s. 5')),
	(6, _('s. 6')),
	(7, _('s. 7')),
	(8, _('s. 8')),
	(9, _('s. 9')),
	(10, _('s. 10')),
	(11, _('s. 11')),
	(12, _('s. 12')),
	(13, _('s. 13')),
	(14, _('s. 14')),
	(15, _('s. 15')),
	(16, _('s. 16')),
)

PAYS = (
	('DZ', _('Algérie')),
        ('DE', _('Allemagne')),
	('EG', _('Egypte')),
	('ES', _('Espagne')),
	('FR', _('France')),
	('GR', _('Grèce')),
        ('IT', _('Italie')),
	('IQ', _('Irak')),
	#('IL', _('Israel-Palestine')),
	('LB', _('Liban')),
	('LY', _('Libye')),
	('MK', _('Macédoine')),
        ('TR', _('Turquie')),
        ('TN', _('Tunisie')),
	('SY', _('Syrie')),
)

class Lieu(models.Model):
    #nom = models.CharField(_('nom_lieu'),max_length=100,null=True,blank=True,help_text=_('Nom original'))
    ville_ancienne = models.CharField(_('ville'),max_length=100,null=True,blank=False)
    nom_trans = models.CharField(_('nom_lieu_trans'),max_length=100,null=True,blank=True,help_text=_('Nom du lieu translittéré'))
    pays = models.CharField(_('pays_actuel'),max_length=5,choices=PAYS,null=True,blank=True,help_text=_('Pays actuel'))
    region = models.CharField(_('region_hist'),max_length=100,null=True,blank=True,help_text=_('Région historique en langue originale'))
    region_trans = models.CharField(_('region_hist_trans'),max_length=100,null=True,blank=True,help_text=_('Région historique translittérée'))
    ville = models.CharField(_('ville_actuelle'),max_length=100,null=True,blank=True,help_text=_('Si différente de la ville historique'))
    lat = models.FloatField(_('lattitude'),null=True,blank=True)
    longi = models.FloatField(_('longitude'),null=True,blank=True)
    pub_date = models.DateTimeField(default=datetime.now)

    class Meta:
        managed = True
        db_table = 'sara_textes_lieu'
        verbose_name = _('Lieu')
        verbose_name_plural = _('Lieux')

    def __unicode__(self):              # __unicode__ on Python 2
        return self.ville_ancienne
