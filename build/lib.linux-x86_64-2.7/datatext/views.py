from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Context, loader

def carte(request):
    template = loader.get_template('carte.html')
    context = Context({
        #'page':'index',
        })
    return HttpResponse(template.render(context))
