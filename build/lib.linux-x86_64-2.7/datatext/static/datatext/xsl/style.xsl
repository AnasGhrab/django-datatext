<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:output method="html" encoding="utf-8" indent="no"/>

  <xsl:template match="/">
    <html>
      <head><title></title>
        <link rel="stylesheet" href="/static/datatext/css/style.css" type="text/css"/>
      </head>
      <body>

<div class="menu">
  <a href="/textes/">قائمة المؤلّفين والنّصوص</a> |
  <a href="/bibliotheques/">قائمة المكتبات</a> |
  <a href="/ecoles/">  المدارس الأساسيّة</a> |
  <a href="/bibliographie/">المراجع</a>
</div>

        <h1><xsl:value-of select="//title[@type='main']"/></h1>
        <h2><xsl:value-of select="//surname"/></h2>
	<p><xsl:value-of select="//abstract"/></p>
        <xsl:apply-templates select="//sourceDesc" />
	<xsl:apply-templates select="//front" />

	<hr/>
 
       <xsl:apply-templates select="//body" />
      </body>
    </html>
  </xsl:template>

  <xsl:template match="//figure">
      <b>
                <xsl:value-of select="head"/>"/>
      </b>
  </xsl:template>

  <xsl:template match="//sourceDesc">
      <ul>
		<xsl:apply-templates select="//witness"/>
      </ul>
  </xsl:template>


  <xsl:template match="//front">
        <xsl:for-each select="div[@type='abstract']">
                <h2><xsl:value-of select="head" /></h2>
                <xsl:for-each select="p">
                        <p><xsl:apply-templates/></p>
                </xsl:for-each>
        </xsl:for-each>	
	<xsl:for-each select="div[@type='contents']">
        	<h2><xsl:value-of select="head" /></h2>
		<ul>
        	<xsl:for-each select="//body/div">
          		<li><xsl:value-of select="head" /></li>
        	</xsl:for-each>
		</ul>
      	</xsl:for-each>
  </xsl:template>

  <xsl:template match="//witness">
	<li>
             <xsl:value-of select="msDesc/msIdentifier/repository" />
                (<xsl:value-of select="msDesc/msIdentifier/settlement" />/<xsl:value-of select="msDesc/msIdentifier/country" />) :
            <b>
            <xsl:value-of select="msDesc/msIdentifier/idno" />
            <xsl:apply-templates select="msDesc/msPart/msIdentifier/idno" />
            </b>
            <xsl:if test="msDesc/msPart/head/title != ''">
                <xsl:value-of select="msDesc/msPart/head/title" />
            </xsl:if>
            <xsl:if test="msDesc/msPart/head/persName/surname != ''">
                (<xsl:value-of select="msDesc/msPart/head/persName/surname" />)
            </xsl:if>
	    <xsl:if test="msDesc/msPart/msContents/summary != ''">
		<p>
            	<xsl:apply-templates select="msDesc/msPart/msContents/summary" />	
		</p>
	    </xsl:if>
	    <xsl:apply-templates select="msDesc/msPart/msContents/msItem" />
	</li>
  </xsl:template>

  <xsl:template match="//msDesc/msPart/msIdentifier/idno">
            <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="msDesc/msPart/msContents/msItem">
                <ul>
                <li>بداية النصّ : <xsl:value-of select="incipit" /></li>
                <li>آخر النصّ : <xsl:value-of select="explicit" /></li>
                </ul>
  </xsl:template>

  <xsl:template match="//ref">
     <xsl:choose>
     <xsl:when test="@target != 'None'">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
     </xsl:when>
     <xsl:otherwise>
            <xsl:apply-templates/>
     </xsl:otherwise>
     </xsl:choose>
  </xsl:template>

  <xsl:template match="//body">
      <xsl:for-each select="div">
        	<h2><xsl:value-of select="head" /></h2>
        	<xsl:for-each select="p">
          		<p><xsl:apply-templates/></p>
		</xsl:for-each>
                <xsl:for-each select="ol">
                        <ul><xsl:apply-templates/></ul>
                </xsl:for-each>
              <xsl:for-each select="div2">
                <h3><xsl:value-of select="head" /></h3>
                	<xsl:for-each select="p">
                  		<p><xsl:apply-templates/></p>
                	</xsl:for-each>
              </xsl:for-each>
      </xsl:for-each>
  </xsl:template>


  <xsl:template match="p">
    <p><xsl:apply-templates/></p>
  </xsl:template>

  <xsl:template match="b">
    <b><xsl:apply-templates/></b>
  </xsl:template>

  <xsl:template match="list">
    <ul>
      <xsl:for-each select="item">
        <li><xsl:apply-templates/></li>
      </xsl:for-each>
    </ul>
  </xsl:template>

  <xsl:template match="li">
        <li><xsl:apply-templates/></li>
  </xsl:template>

  <xsl:template match="note">
    <span class="note_bas">*
      <span class="afficher">
        <xsl:apply-templates/>
      </span>
    </span>
  </xsl:template>

  <xsl:template match="app">
    <span class="apparat"><xsl:value-of select="lem"/>
      <span class="afficher">
        <xsl:for-each select="rdg"><xsl:value-of select="@wit"/> : <xsl:apply-templates/><br/></xsl:for-each>
      </span>
    </span>
  </xsl:template>

</xsl:stylesheet>
