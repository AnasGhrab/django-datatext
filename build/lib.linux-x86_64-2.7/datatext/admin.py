#!/usr/local/bin/python
# -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.contrib import admin
from models.models import *
from .models.dateslieux import *
from django.utils.translation import ugettext as _

from django import forms
from codemirror import CodeMirrorTextarea
#from ckeditor.widgets import CKEditorWidget

admin.site.site_header = _('Modification des données')
codemirror_widget = CodeMirrorTextarea(
	addon_css = ('fold/foldgutter','hint/show-hint',),
	addon_js = ('edit/closetag', 'fold/xml-fold', 'edit/matchtags', 'fold/foldcode', 'fold/foldgutter','hint/show-hint','hint/xml-hint','hint/html-hint',),
	mode='xml',  dependencies=('javascript', 'css', 'htmlmixed'),
	theme = 'default',
	config={ 'lineNumbers': True,
		 'mode': "text/html",
		 'lineWrapping': True,
		 'autoCloseTags': True,
		 'matchTags': {'bothTags': True},
		 'foldGutter': True,
    		 'gutters': ["CodeMirror-linenumbers", "CodeMirror-foldgutter"],
    		 'extraKeys': {
#			"Ctrl-J": "toMatchingTag",
#   			"'<'": 'completeAfter',
#          		"'/'": 'completeIfAfterLt',
#          		"' '": 'completeIfInTag',
#          		"'='": 'completeIfInTag',
          		"Ctrl-Space": "autocomplete",
			},
		'value' : 'document.documentElement.innerHTML',
		'hintOptions': {'schemaInfo': { 
		       #"!top": ["p"],
        	       #"!attrs": {
          		#	'id' : 'null',
          		#	'class': ["A", "B", "C"]
       	 		#	},
			'div2' : {
				'children': ["head", "p"]
				},
			'head' : {
				'children': ['null']				
				},
        		'p' : {
          			'attrs': {
            				'lang': ["en", "de", "fr", "nl"],
            				'freeform': 'null'
          				},
          			'children': ["note", "lemma"]
        			},
        		'note': {
          			'attrs': {
            				'name': 'null',
            				'isduck': ["yes", "no"]
          				},
          			'children': ["wings", "feet", "body", "head", "tail"]
        			},
			'app': {
				'children': ['lem']
			    },
        		'lem': {
          			'attrs': {'name': 'null'},
          			'children': ['rdg']
        			},
			'rdg' : {
				'attrs': {
					'wit': [''],
					},
				},
			    },
			},
		}, #END config=
	)

class SectionInline(admin.StackedInline):
    model = Section
    formfield_overrides = { 
                models.TextField: {'widget': codemirror_widget},
        }
 
class RefInline(admin.StackedInline):
    model = Reference
    extra = 2

class BibInline(admin.StackedInline):
    model = RefBib

class ResponsableInline(admin.StackedInline):
    model = Responsable
    extra = 1

class TextAdmin(admin.ModelAdmin):
	list_display = ['titre_orig','auteur','siecle','annee','precision','lieu']
	ordering = ['siecle','auteur']
	list_filter = ('categorie','tags','siecle','lieu',)
	search_fields = ['titre_orig','auteur__nom','tags__name']
	inlines = [ ResponsableInline, RefInline, SectionInline, BibInline, ]
        formfield_overrides = {
        	models.ManyToManyField: {'widget': forms.CheckboxSelectMultiple},
		models.TextField: {'widget': codemirror_widget},
        }

        @property
        def media(self):
                media = super(TextAdmin, self).media
                css = { 
                        "all": ("datatext/css/extra.css",)
                        }   
                media.add_css(css)
                return media

class SectionAdmin(admin.ModelAdmin):
        search_fields = ['texte__titre_orig','texte__auteur__nom','texte__auteur__prenom']
        formfield_overrides = { 
                models.TextField: {'widget': codemirror_widget},
        }   

        @property
        def media(self):
        	media = super(SectionAdmin, self).media
        	css = {
            		"all": ("datatext/css/extra.css",)
        		}
       		js = [
           		"datatext/js/tags.js",
        	      ]
        	media.add_css(css)
		media.add_js(js)
        	return media

class AuteurAdmin(admin.ModelAdmin):
        search_fields = ['nom','prenom']

class ResponsableAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

class FonctionAdmin(admin.ModelAdmin):
    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        """
        return {}

class CatalogueAdmin(admin.ModelAdmin):
        list_display = ['intitule','abbrev']

admin.site.register(Auteur,AuteurAdmin)
admin.site.register(Responsable,ResponsableAdmin)
admin.site.register(Fonction,FonctionAdmin)
admin.site.register(Lieu)
admin.site.register(Texte,TextAdmin)
admin.site.register(Catalogue,CatalogueAdmin)
admin.site.register(Copie)
admin.site.register(Section,SectionAdmin)
admin.site.register(Categorie)
admin.site.register(Corpus)
admin.site.register(Bibliotheque)
admin.site.register(Bibliographie)
admin.site.register(Avancement)
