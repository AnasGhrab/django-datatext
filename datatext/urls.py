from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from datatext import views

urlpatterns = [
#    path('api/auteurs/', views.AuteurList.as_view()),
#    path('api/textes/', views.TexteList.as_view()),
#    path('api/auteurs/<int:pk>/', views.AuteurDetail.as_view()),
#    path('api/textes/<int:pk>/', views.TexteDetail.as_view()),
#    path('api/users/', views.UserList.as_view()),
#    path('api/users/<int:pk>/', views.UserDetail.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
