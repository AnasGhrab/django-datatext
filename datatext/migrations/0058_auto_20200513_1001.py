# Generated by Django 3.0.5 on 2020-05-13 09:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0057_remove_auteur_owner'),
    ]

    operations = [
        migrations.AddField(
            model_name='ville',
            name='nom_actuel',
            field=models.CharField(blank=True, help_text='Nom actuel de la ville', max_length=20, null=True, verbose_name='nom_ville'),
        ),
        migrations.AddField(
            model_name='ville',
            name='nom_en_actuel',
            field=models.CharField(blank=True, help_text='Nom actuel de la ville - en', max_length=20, null=True, verbose_name='nom_ville_en'),
        ),
        migrations.AddField(
            model_name='ville',
            name='nom_fr_actuel',
            field=models.CharField(blank=True, help_text='Nom actuel de la ville - fr', max_length=20, null=True, verbose_name='nom_ville_fr'),
        ),
        migrations.AlterField(
            model_name='ville',
            name='nom',
            field=models.CharField(blank=True, help_text='Nom ancien de la ville', max_length=20, null=True, verbose_name='nom_ville'),
        ),
        migrations.AlterField(
            model_name='ville',
            name='nom_en',
            field=models.CharField(blank=True, help_text='Nom ancien de la ville - en', max_length=20, null=True, verbose_name='nom_ville_en'),
        ),
        migrations.AlterField(
            model_name='ville',
            name='nom_fr',
            field=models.CharField(blank=True, help_text='Nom ancien de la ville - fr', max_length=20, null=True, verbose_name='nom_ville_fr'),
        ),
    ]
