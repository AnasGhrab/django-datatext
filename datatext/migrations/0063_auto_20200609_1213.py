# Generated by Django 3.0.6 on 2020-06-09 11:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0062_auto_20200601_2131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='icono',
            name='folio',
            field=models.CharField(blank=True, max_length=10, null=True),
        ),
    ]
