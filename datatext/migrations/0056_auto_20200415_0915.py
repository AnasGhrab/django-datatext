# Generated by Django 2.2.9 on 2020-04-15 08:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0055_section_ordre'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='section',
            options={'managed': True, 'ordering': ['ordre'], 'verbose_name': 'جزء', 'verbose_name_plural': 'الأجزاء'},
        ),
    ]
