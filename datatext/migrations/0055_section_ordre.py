# Generated by Django 2.2.9 on 2020-04-15 08:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('datatext', '0054_auto_20200320_1347'),
    ]

    operations = [
        migrations.AddField(
            model_name='section',
            name='ordre',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
