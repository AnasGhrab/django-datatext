#!/usr/local/bin/python
  # -*- coding: utf-8 -*-

from __future__ import unicode_literals
from django.apps import AppConfig
from django.utils.translation import ugettext as _

class DataTextConfig(AppConfig):
    name = 'datatext'
#    verbose_name = _('DaTaText')

#class AuteursConfig(AppConfig):
#    name = 'auteurs'
