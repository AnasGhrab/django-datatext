from django.shortcuts import render, redirect
from django.http import HttpResponse, Http404
from django.template import Context, loader

#from django.views.decorators.csrf import csrf_exempt
#from rest_framework.parsers import JSONParser

from rest_framework import generics
from rest_framework import permissions
from datatext.models.models import Auteur, Texte
from datatext.serializers import AuteurSerializer, TexteSerializer, UserSerializer

from django.contrib.auth.models import User

def carte(request):
    template = loader.get_template('carte.html')
    context = Context({
        #'page':'index',
        })
    return HttpResponse(template.render(context))

class TexteList(generics.ListCreateAPIView):
    queryset = Texte.objects.all()
    serializer_class = TexteSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class TexteDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Texte.objects.all()
    serializer_class = TexteSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class AuteurList(generics.ListCreateAPIView):
    queryset = Auteur.objects.all()
    serializer_class = AuteurSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

class AuteurDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Auteur.objects.all()
    serializer_class = AuteurSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

class UserList(generics.ListAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer
