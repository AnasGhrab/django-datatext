from django.conf import settings

from pyzotero import zotero

def _get_zotero(request, texte_id):
  #libraddry_id = 172761
  #library_type = 'group'
  #api_key = 'vkLtPNKLDYYmprBmeyV16s80'
  zot = zotero.Zotero(settings.zotero_library_id, settings.zotero_library_type, settings.zotero_api_key)
  items = []
  references = m.Bibliographie.objects.filter(texte_id=texte_id)
  for reference in references:
    zot.add_parameters(content='bib', style='mla')
    item_ref = reference.zotero
    items.append(zot.item(item_ref)[0])
  return items
