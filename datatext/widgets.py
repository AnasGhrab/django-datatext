from django import forms
from django.conf import settings

#if (settings.LANGUAGE_CODE == 'ar'):
#    extracss = '/static/datatext/css/codemirror-rtl.css'
#else:
#    extracss = ''

class TextInputAdmin(forms.TextInput):
    def __init__(self, *args, **kwargs):
        super(TextInputAdmin, self).__init__(*args, **kwargs)
        self.attrs['style'] = 'width:80%'

class InputLTRAdmin(TextInputAdmin):
    def __init__(self, *args, **kwargs):
        super(InputLTRAdmin, self).__init__(*args, **kwargs)
        self.attrs['style'] = 'direction:ltr; width:80%;'

class HtmlEditor(forms.Textarea):
    def __init__(self, *args, **kwargs):
        super(HtmlEditor, self).__init__(*args, **kwargs)
        self.attrs['class'] = 'html-editor'

    class Media:
        css = {
            'all': (
                '/static/codemirror-5.49.0/lib/codemirror.css',
                '/static/codemirror-5.49.0/addon/fold/foldgutter.css',
                '/static/codemirror-5.49.0/addon/hint/show-hint.css',
                '/static/datatext/css/codemirror-rtl.css'
            )
        }
        js = (
            '/static/codemirror-5.49.0/lib/codemirror.js',
            '/static/codemirror-5.49.0/mode/xml/xml.js',
            '/static/codemirror-5.49.0/mode/htmlmixed/htmlmixed.js',
            '/static/codemirror-5.49.0/addon/fold/foldcode.js',
            '/static/codemirror-5.49.0/addon/fold/foldgutter.js',
            '/static/codemirror-5.49.0/addon/fold/xml-fold.js',
            '/static/codemirror-5.49.0/addon/edit/matchtags.js',
            '/static/codemirror-5.49.0/addon/edit/closetag.js',
            '/static/codemirror-5.49.0/addon/hint/show-hint.js',
            '/static/codemirror-5.49.0/addon/hint/xml-hint.js',
            '/static/datatext/js/codemirror-init.js'
        )

class LTRTextAdmin(forms.Textarea):
    def __init__(self, *args, **kwargs):
        super(LTRTextAdmin, self).__init__(*args, **kwargs)
        self.attrs['style'] = 'direction:ltr; width:80%'
        #self.attrs['class'] = 'html-editor'
