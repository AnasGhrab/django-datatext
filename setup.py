# -*- coding: utf-8 -*-

import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name='django-datatext',
    version='0.46',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
#	'django-taggit',
        'jsonfield',
#        'django-geojson', # Pas besoin
#        'django-leaflet',
#	'pyzotero',
#	'django-ckeditor', # Abandonné
#        'django-codemirror',
        'django-bibliographic',
#       'django-codemirror-widget', # code intégré dans les static files
    	],
    license='MIT License',
    description='A Django app to manage sources-texts and critical editions (mainly arabic).',
    long_description=README,
    url='https://anas.ghrab.tn/',
    author='Anas Ghrab',
    author_email='anas.ghrab@gmail.com',
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: 2.1',  # replace "X.Y" as appropriate
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',  # example license
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        # Replace these appropriately if you are stuck on Python 2.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],
)
