========
DaTaText
========

DaTaText is a django-python app to manage digital texts from manuscripts, mainly as a catalogue and critical editions in the TEI format.

Quick start
-----------

1. Add "datatext" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = (
        ...
        'datatext',
    )

3. Run `python manage.py makemigrations` then `python manage.py migrate` to create datatext models.

4. Add the default languages for textes : LANG_TXT_DEFAULT = 'fr'.

5. Start the development server and visit http://127.0.0.1:8000/admin/ (you'll need the Admin app enabled).


Contact
=======

Homepage: http://anas.ghrab.tn

Email:

 * Anas Ghrab <anas.ghrab@gmail.com>

License
=======

.. image:: img/by-nc-sa1.png
   :height: 40
   :target: https://creativecommons.org/licenses/by-nc-sa/3.0/
